      <div id="dzi1" style="height: 600px; background-color: black;"></div>

      <div id="dzi-control" class="ui bottom attached label">
          <button id="resetZoom" class="ui button"><i class="fa fa-arrows-alt"></i> <span class="hidden-xs">tilpass zoom</span></button>
          <button id="full-page" class="ui button" alt="Fullskjermvisning av og på"><i id="fullPage" class="fa fa-expand"></i></button>
          <button id="zoom-out" class="ui button"><i class="fa fa-search-minus"></i></button>
          <button id="zoom-in" class="ui button"><i class="fa fa-search-plus"></i></button>
          {% models.dzi|length > 1  %}
          <button id="previous" class="ui button"><i class="fa fa-arrow-left"></i> forrige</button>
          <button>gå til side &nbsp;<input style="width: 55px;" type="number" id="page" value="1"/> av <span id="tileSourcesLength"></span></button>
          <button id="next" class="ui button">neste <i class="fa fa-arrow-right"></i></button>
          {%endif%}
      </div>

      <script type="text/javascript" src="{{lodspk.home}}js/openseadragon/openseadragon.js"></script>

      <script type="text/javascript"> 
        var viewer = OpenSeadragon({
          id:            "dzi1",
          prefixUrl:     "{{lodspk.home}}vjs/openseadragon/images/",
          tileSources:   [     
          {% for row in models.dzi %}{%if !forloop.first && not models.dzi| null %},{%endif%}
          "{{ row.dziUri.value }}"{% endfor %}
          ],
          preserveViewport: false,
          minZoomImageRatio: 0.6,
          minPixelRatio: 0.5,
          animationTime: 0.8,
          springStiffness: 8,
          showNavigator:  true,
          //navigatorId:   "navigatorDiv",
          toolbar:       "dzi-control",
          zoomInButton: "zoom-in",
          zoomOutButton:  "zoom-out",
          homeButton:     "resetZoom",
          fullPageButton: "full-page",
          nextButton:     "next",
          previousButton: "previous"
        });

        $('#page').change(function() {
          chpage = $('#page').val();
          currentPage = chpage-1;
          viewer.goToPage(chpage-1); 
        });
        viewer.addHandler('page', function(event){
          $('#page').val(event.page+1);
        });
        function tileSourceLength() {
          length = viewer.tileSources.length ;
          $('#tileSourcesLength').append(length);
        };
        tileSourceLength();
    </script>
