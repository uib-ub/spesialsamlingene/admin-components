<div class="ui raised vertical sidebar accordion menu uncover push responsive">

  <div class="item">
    <div class="ui input">
      <form class="ui form" role="search" method="get" action="/search">
        <input type="text" placeholder="Søk ..." name="q">
      </form>
    </div>
  </div>

  <a class="item" href="{{lodspk.home}}home">
    <i class="home icon"></i> Hjem
  </a>

  <a class="item" href="{{lodspk.home}}cataloguers">
    <i class="image icon"></i> Siste 500
  </a>

  <a class="title">
    Katalogisér <i class="flag icon"></i>
  </a>
  <div class="content">
    <a class="item" href="/upload/">
      <i class="upload icon"></i> Last opp
    </a>
    <a class="item" href="{{lodspk.home}}search/?filter=isCatalogued%23Ikke%20katalogisert">
      <i class="edit icon"></i> Registrer
    </a>
    <a class="item" href="{{lodspk.home}}search/?filter=showWeb%23F">
      <i class="newspaper icon"></i> Publiser
    </a>
    <a class="item" href="{{lodspk.home}}search/?filter=hasZoom%23Med%20DeepZoom&filter=showWeb%23F">
      <i class="gift icon"></i> Nye filer
    </a>
  </div>

  <a class="item" href="{{lodspk.home}}orders-printlabel">
    <i class="newspaper icon"></i> Etiketter
  </a>

  <a class="title">
    Oppdrag<i class="shopping bag icon"></i>
  </a>
  <div class="content">
    <a class="item" href="{{lodspk.home}}orders">
      <i class="search icon"></i>Søk i oppdrag
    </a>
    <a class="item" href="http://oppdrag.ub.uib.no" target="_blank">
      <i class="fa fa-bullseye fa-fw"></i> Oppdrag.uib.no
    </a>
  </div>

  <a class="title">
    Bla i...<i class="browser icon"></i> 
  </a>
  <div class="content">
    <a class="item" href="{{lodspk.home}}collections">
      <i class="grid layout icon"></i>Samlinger
    </a>
    <a class="item" href="{{lodspk.home}}events">
      <i class="calendar icon"></i>Hendelser
    </a>
    <a class="item" href="{{lodspk.home}}exhibitions">
      <i class="university icon"></i>Utstillinger
    </a>
    <a class="item" href="{{lodspk.home}}conceptschemes">
      <i class="tags icon"></i>Register
    </a>
    <a class="item" href="{{lodspk.home}}places">
      <i class="map pin icon"></i>Steder
    </a>
    <a class="item" href="{{lodspk.home}}techniques">
      <i class="book icon"></i>Teknikker
    </a>
    <a class="item" href="{{lodspk.home}}topics-hierarchy">
      <i class="tags icon"></i>BS register
    </a>
    <a class="item" href="{{lodspk.home}}worklists">
      <i class="ordered list icon"></i>Arbeidslister
    </a>
    <a class="item" href="{{lodspk.home}}acquisitions">
      <i class="gift icon"></i>Akkvisisjoner
    </a>
    <a class="item" href="{{lodspk.home}}storageunits">
      <i class="disk outline icon"></i>Lagringsenheter
    </a>  
  </div>

  <a class="title">
    Statistikk <i class="line chart icon"></i>
  </a>
  <div class="content">
    <a class="item" href="{{lodspk.home}}marcus-statistics">
      <i class="line chart icon"></i> Stat Marcus
    </a>
    <a class="item" href="{{lodspk.home}}stats">
      <i class="line chart icon"></i> Stat datasettet
    </a>
  </div>

  <a class="item" href="https://ubblod.disqus.com/admin/moderate/" target="_blank">
    <i class="comments icon"></i> Disqus
  </a>

  <a class="item" href="{{lodspk.home}}manual">
    <i class="book icon"></i> Veiledning
  </a>

  <a class="item" href="http://marcus.uib.no" target="_blank">
    <i class="bullseye icon"></i> Marcus
  </a>

  <a class="item" href="https://sak.uib.no/projects/pw0/issues/new" target="_blank">
    <i class="exclamation triangle icon"></i>Feil
  </a>
</div>
