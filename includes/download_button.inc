{% if models.digitalresources|length != 0 %}    
<div class="ui small buttons">
  <div class="ui button"><i class="fa fa-download"></i> Last ned</div>
  <div class="ui floating dropdown icon button download-dropdown">
    <i class="dropdown icon"></i>
    <div class="menu">
      {% if first.digitalresources.imgXS.value %}
      <div class="item"><a href="{{first.digitalresources.imgXS.value}}" download>JPG - veldig liten</a></div>
      {% endif %}
      {% if first.digitalresources.imgSM.value %}
      <div class="item"><a href="{{first.digitalresources.imgSM.value}}" download>JPG - liten</a></div>
      {% endif %}
      {% if first.digitalresources.imgMD.value %}
      <div class="item"><a itemprop="image" href="{{first.digitalresources.imgMD.value}}" download>JPG - medium</a></div>
      {% endif %}
      {% if first.digitalresources.imgLG.value %}
      <div class="item"><a itemprop="image" href="{{first.digitalresources.imgLG.value}}" download>JPG - stor</a></div>
      {% endif %}
    </div>
  </div>
</div>
{% endif %}