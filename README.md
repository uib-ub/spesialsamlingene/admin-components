# Admin components


This repository contains all queries, templates and extra views for the Marcus administrative webpage (admin.marcus.uib.no). The frontend framework used is [Semantic-UI](https://semantic-ui.com/) and Haanga, a django like framework.

This repo runs CI and builds the latest commit to [marcus admin test server](https://admin.marcus.ubbe.no/home) on commit where changes can be viewed, , and also contains deployment to the production server.


A possible workflow is: 
* [open an issue](https://git.app.uib.no/uib-ub/spesialsamlingene/admin-components/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
* create merge request (from issue in [web-GUI](https://git.app.uib.no/uib-ub/spesialsamlingene/admin-components/-/issues))
* push to merge request
* review/confirm changes/feature in [the test server](https://admin.marcus.ubbe.no/home)
* merge request to master
* Deploy to [the production server](https://admin.marcus.uib.no/home) by clicking on the `deploy_prod` button after the `build` and `deploy` jobs have succesfully completed for the master branch.

![Image of deployment](docs/deploy_prod.png)

## Related projects

* [ansible-marcus](https://git.app.uib.no/uib-ub/spesialsamlingene/ansible-marcus) sets up the marcus infrastructure, including admin endpoint.
* [marcus search client (**admin** branch)](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-search-client/-/tree/admin) is the elasticsearch client.
* [lodspeakr](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-lodspeakr) is the framework which uses the components. The components repo is usually placed in the components in the main directory. 
* [marcus-components](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-components), [katalog.skeivtarkiv.no](https://git.app.uib.no/uib-ub/skeivtarkiv/katalog.skeivtarkiv.no) are other components repos

## Semantic UI
Configuration files used to change components

semantic.json
./static/themes/default.theme.config


## Install Blackbox-search / Admin
The admin search contains a similar CI configuration in [marcus-search-client](https://git.app.uib.no/uib-ub/spesialsamlingene/marcus-search-client/-/tree/admin) where changes will be automitcally deployed to test, while prod can be updated from the UI.

## Local development/deployment

At the moment building `admin components` requires Node 6 which is past end of life support.
It may be hard to set up locally.

Inspect the [.gitlab-ci.yml](.gitlab-ci.yml) to view build steps.

### Old Installation instructions (not working?)

Configuration files used to change components

semantic.json
./static/themes/default.theme.config

Edit theme in static/themes/uib. folder `uib` is then copied to `static/semantic/src/themes` before building

```
npm install 
cd static/semantic/
npx gulp build
```