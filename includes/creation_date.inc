{% if first.main.created.value != null %}
<p><strong><i class="fa fa-calendar"></i> 
  Dato: <span itemprop="dateCreated">{{ first.main.created.value }}</span>
</strong></p>
{% endif %}

{% if not first.main.madeafter.value | null || not first.main.madebefore.value | null %}
<p><i class="fa fa-calendar"></i> 
  {% if not first.main.madeafter.value | null && not first.main.madebefore.value | null %}
  Laget mellom <strong>{{ first.main.madeafter.value }}</strong> og <strong>{{ first.main.madebefore.value }}</strong>
  {% endif %}
  {% if not first.main.madeafter.value | null && first.main.madebefore.value | null %}
  Laget etter <strong>{{ first.main.madeafter.value }}</strong>
  {% endif %}
  {% if first.main.madeafter.value | null && not first.main.madebefore.value | null %}
  Laget før <strong>{{ first.main.madebefore.value }}</strong>
  {% endif %}
</p>
{% endif %}
