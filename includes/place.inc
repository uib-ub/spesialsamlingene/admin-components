            
{% if first.main.hasLong.value == "false" %}
<h3 class="panel-title">Tilknyttede steder</h3>
{% endif %}

{% if first.main.hasLong.value == "true" %}
<div style="height: 400px; width: 100%;" id="map"></div>
{% endif %}
<div class="ui message">
  <div class="ui horizontal list">
    {% for row in models.places %}
    {% ifchanged row.uri.value %}
    <div class="item"><i class="fa fa-map-marker"></i> <a href="{{ row.uri.value }}">{{row.label.value}}</a></div>
    {% endifchanged %}
    {% endfor %} 
  </div>
</div>
  
<script>            
  var map = L.map('map');
  var defaultLayer = L.tileLayer.provider('OpenStreetMap.Mapnik').addTo(map);
  var baseLayers = [ 'OpenStreetMap.Mapnik', 'MapQuestOpen.OSM', 'MapQuestOpen.Aerial', 'Stamen.Watercolor' ];
  var overlayLayers = [ 'Kartverket', 'Rodekart9', 'Rodekart4', 'Rodekart1' ];
  L.control.layers.provided(baseLayers, overlayLayers, {collapsed: true}).addTo(map);
  {% for row in models.places %}{% if row.long.value != null %}
  L.marker([{{ row.lat.value }}, {{ row.long.value }}]).addTo(map).bindPopup("<b><a href=\"{{ row.uri.value }}\">{{row.label.value}}</a></b>").openPopup();
  {% endif %}{% endfor %} 
  map.fitBounds([{% for row in models.places %}{%if !forloop.last && row.lat.value != null%}, {%endif%}{% if row.lat.value != null %}[{{ row.lat.value }}, {%endif%}{% if row.long.value != "" %}{{ row.long.value }}]{%endif%}{% endfor %}], {padding: [70, 70]});
  map.touchZoom.disable();
  map.scrollWheelZoom.disable(); 
  map.invalidateSize();    
</script>