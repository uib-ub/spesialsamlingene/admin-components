{% if models.dzi | length > 0 %}
	{% set dzi_enabled = 'true' %}
	{% set dzi_dr = 'true' %}
	
{% endif %}
{% if models.digitalresources | length > 0 %}
  {% set dr_enabled = 'true' %}
  {% set dzi_dr = 'true' %}
{% endif %}
      {% if dzi_enabled == 'true' %}
      <!-- Contains the OpenSeadragon viewer -->
      <div class="ui top attached tab segment no-padding active" data-tab="dzi"> 
        <div id="dzi1" style="height:50rem; background-color: black;"></div>
      </div> 
      {% endif %}   

      {% if models.digitalresources| length > 0 %}
      <div data-tab="jpeg" class="{% if not dzi_enabled == 'true' %}active{% endif %} ui top attached tab segment no-padding">

        <!-- fotorama.css & fotorama.js. -->
        <link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.3/fotorama.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.3/fotorama.js"></script>

        <div class="fotorama">
          {% for row in models.digitalresources %}    
            <a href="{{row.imgSM.value}}" data-full="{{row.imgMD.value}}"></a>
          {% endfor %}
        </div>

      </div>
      {% endif %}
      
          {% if dzi_enabled == 'true' &&  dr_enabled == 'true' %}
    <div class="ui bottom attached tabular menu dr-tab">
      <a class="item {% if dzi_enabled == 'true' %}active{% else %}disabled{% endif %}" data-tab="dzi">Zoom</a>
      <a class="item {% if not dzi_enabled == 'true' %}active{% endif %}" data-tab="jpeg">Jpeg</a>
    
    
    <div id="dzi-control" class="right menu">
          <button id="resetZoom" class="ui item button"><i class="fa fa-arrows-alt"></i> <span class="hidden-xs">tilpass zoom</span></button>
          <button id="full-page" class="ui item button" alt="Fullskjermvisning av og på"><i id="fullPage" class="fa fa-expand"></i></button>
          <button id="zoom-out" class="ui item button"><i class="fa fa-search-minus"></i></button>
          <button id="zoom-in" class="ui item button"><i class="fa fa-search-plus"></i></button>
          {% if models.dzi|length > 1 %}
          <button id="previous" class="ui item button"><i class="fa fa-arrow-left"></i> forrige</button>
          <button class="ui item button">gå til side &nbsp;<input style="width: 55px;" type="number" id="page" value="1"/> av <span id="tileSourcesLength"></span></button>
          <button id="next" class="ui item button">neste <i class="fa fa-arrow-right"></i></button>
          {%endif%}
        </div>
      </div>  
    {% endif %}
    
           <script src="{{lodspk.home}}node_modules/openseadragon/build/openseadragon/openseadragon.min.js"></script>


      <script type="text/javascript"> 
        var viewer = OpenSeadragon({
          id:            "dzi1",
          prefixUrl:     "{{lodspk.home}}node_modules/openseadragon/build/openseadragon/images/",
          crossOriginPolicy: "Anonymous",
          tileSources:   [     
          {% for row in models.dzi %}{%if !forloop.first && models.dzi|length > 1 %},{%endif%}
          "{{ row.dziUri.value }}"{% endfor %}
          ],
          visibilityRatio: 1.0,
          minZoomLevel: 0.3,
          constrainDuringPan: true,
          preserveViewport: false,
          //minZoomImageRatio: 0.6,
          minPixelRatio: 0.5,
          animationTime: 0.8,
          springStiffness: 8,
          sequenceMode: true,
          showRotationControl: true,
          showReferenceStrip: false,
          showNavigator:  true,
          //navigatorId:   "navigatorDiv",
          toolbar:       "dzi-control",
          zoomInButton: "zoom-in",
          zoomOutButton:  "zoom-out",
          homeButton:     "resetZoom",
          fullPageButton: "full-page",
          nextButton:     "next",
          previousButton: "previous"
        });

        $('#page').change(function() {
          chpage = $('#page').val();
          currentPage = chpage-1;
          viewer.goToPage(chpage-1); 
        });
        viewer.addHandler('page', function(event){
          $('#page').val(event.page+1);
        });
        function tileSourceLength() {
          length = viewer.tileSources.length ;
          $('#tileSourcesLength').append(length);
        };
        tileSourceLength();
    </script>
