{%if models.po | length > 0 %}
<div class="ui segment">
  <div class="header">
    Denne siden har disse egenskapene
  </div>
  <table class="ui fluid very compact very basic large table" about="{{uri}}">
    <thead>
      <tr><th>Predikat</th><th>Objekt</th></tr>
    </thead>
    {% for row in models.po %}
    <tr>
      <td class="collapsing">{% ifchanged row.p.value %}<a href="{{row.p.value}}">{% if row.classLabel %}{{row.classLabel.curie}}{% else %}{{row.p.value}}{%endif%}</a>{% endifchanged%}</td>
      <td>
        {%if  not row.o.uri | null && row.label %}
        <a rel='{{row.p.value}}' href='{{row.o.value}}'>{{row.label.value}}</a>
        {%endif%}
        {%if  not row.o.uri | null  && !row.label %}
        <a rel='{{row.p.value}}' href='{{row.o.value}}'>{{row.o.curie}}</a>
        {%endif%}
        {%if  row.o.uri | null  && !row.label %}
        <span property='{{row.p.value}}'>{{row.o.value}}</span>
        {%endif%}
      </td>
    </tr>
    {% endfor %}
    <tfoot>
      <tr><th>Predikat</th><th>Objekt</th></tr>
    </tfoot>
  </table>
</div>
{%endif%}
