          <div style="overflow:visible !important;" class="ui visible left vertical inverted sidebar labeled icon menu mobile uihidden left-sidebar">

            <a class="item" href="{{lodspk.home}}home">
              <i class="home icon"></i> Hjem
            </a>

            <a class="item" href="{{lodspk.home}}cataloguers">
              <i class="image icon"></i> Siste
            </a>

            <div class="ui dropdown item">
              <i class="flag icon"></i>Katalogisér
              <div class="menu side-nav-popout">
                <a class="item" href="/upload/">
                  <i class="upload icon"></i> Last opp
                </a>
                <a class="item" href="{{lodspk.home}}search/?filter=isCatalogued%23Ikke%20katalogisert">
                  <i class="edit icon"></i> Registrer
                </a>
                <a class="item" href="{{lodspk.home}}search/?filter=showWeb%23F">
                  <i class="newspaper icon"></i> Publiser
                </a>
                <a class="item" href="{{lodspk.home}}search/?filter=hasZoom%23Med%20DeepZoom&filter=showWeb%23F">
                  <i class="gift icon"></i> Nye filer
                </a>
              </div>
            </div>

            <a class="item" href="{{lodspk.home}}orders-printlabel">
              <i class="newspaper icon"></i> Etiketter
            </a>

            <div class="ui dropdown item">
              <i class="shopping bag icon"></i>Oppdrag
              <div class="menu side-nav-popout">
                <a class="item" href="{{lodspk.home}}orders">
                  <i class="search icon"></i>Søk i oppdrag
                </a>
                <a class="item" href="http://oppdrag.ub.uib.no" target="_blank">
                  <i class="fa fa-bullseye fa-fw"></i> Oppdrag.uib.no
                </a>
              </div>
            </div>

            <div class="ui dropdown item">
              <i class="browser icon"></i>
              Bla i...
              <div class="menu side-nav-popout">
                <a class="item" href="{{lodspk.home}}collections">
                  <i class="grid layout icon"> </i> Samlinger
                </a>
                <a class="item" href="{{lodspk.home}}events">
                  <i class="calendar icon"></i> Hendelser
                </a>
                <a class="item" href="{{lodspk.home}}exhibitions">
                  <i class="university icon"></i> Utstillinger
                </a>
                <hr />
                <a class="item" href="{{lodspk.home}}conceptschemes">
                  <i class="tags icon"></i> Register
                </a>
                <a class="item" href="{{lodspk.home}}places">
                  <i class="map pin icon"></i> Steder
                </a>
                <a class="item" href="{{lodspk.home}}techniques">
                  <i class="book icon"></i> Teknikker
                </a>
                <a class="item" href="{{lodspk.home}}topics-hierarchy">
                  <i class="tags icon"></i> BS register
                </a>
                <hr />
                <a class="item" href="{{lodspk.home}}worklists">
                  <i class="ordered list icon"></i> Arbeidslister
                </a>
                <a class="item" href="{{lodspk.home}}acquisitions">
                  <i class="gift icon"></i> Akkvisisjoner
                </a>
                <a class="item" href="{{lodspk.home}}storageunits">
                  <i class="disk outline icon"></i> Lagringsenheter
                </a>    
              </div>
            </div>

            <div class="ui dropdown item">
              <i class="line chart icon"></i>Stat
              <div class="menu side-nav-popout">
                <a class="item" href="{{lodspk.home}}marcus-statistics">
                  <i class="line chart icon"></i> Stat Marcus
                </a>
                <a class="item" href="{{lodspk.home}}stats">
                  <i class="line chart icon"></i> Stat datasettet
                </a>
              </div>
            </div>

            <a class="item" href="https://ubblod.disqus.com/admin/moderate/" target="_blank">
              <i class="comments icon"></i> Disqus
            </a>

            <a class="item" href="{{lodspk.home}}manual">
              <i class="book icon"></i> Veiledning
            </a>

            <a class="item" href="http://marcus.uib.no" target="_blank">
              <i class="bullseye icon"></i> Marcus
            </a>
            <a class="item" href="https://sak.uib.no/projects/pw0/issues/new" target="_blank">
              <i class="exclamation triangle icon"></i>Feil
            </a>

        <!--  <a class="item" href="{{lodspk.home}}errors">
            <i class="exclamation triangle icon"></i> Feil
          </a>-->

        </div>
