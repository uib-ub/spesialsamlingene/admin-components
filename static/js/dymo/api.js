// namespace
window.semantic = {
    handler: {}
};

$.fn.api.settings.debug = true;

/* Define API endpoints once globally */
$.fn.api.settings.api = {
    "get invoice": "https://oppdrag.ub.uib.no/invoices/{value}.json?key={apikey}",
};

function findUrls(text) {
    var source = (text || '').toString();
    var urlArray = [];
    var url;
    var matchArray;
    // Regular expression to find FTP, HTTP(S) and email URLs.
    var regexToken = /(((ftp|https?):\/\/)[\-\w@:%_\+.~#?,&\/\/=]+)/g;
    // Iterate through any URLs in the text.
    while ((matchArray = regexToken.exec(source)) !== null) {
        var token = matchArray[0];
        var regexComma = new RegExp(',', 'g');
        var regexHtml = new RegExp('.html', 'g');
        //replace via regex
        token = token.replace(regexComma, '');
        token = token.replace(regexHtml, '');
        urlArray.push(token);
    }
    return urlArray;
};

function getJsonld(data) {
    var sparql = "http://sparql.ub.uib.no/sparql/sparql?query=";
    var pre = "prefix%20foaf%3A%20%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2F%3E%0Aprefix%20dct%3A%20%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2F%3E%0Aprefix%20owl%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F2002%2F07%2Fowl%23%3E%0Aprefix%20rdfs%3A%20%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23%3E%0Aprefix%20ubbont%3A%20%3Chttp%3A%2F%2Fdata.ub.uib.no%2Fontology%2F%3E%0A%0ACONSTRUCT%20%7B%0A%20%3Fs%20dct%3Aidentifier%20%3Fidentifier%20%3B%20%0A%20ubbont%3AhasThumbnail%20%3FhasThumbnail%20%3B%0A%20dct%3Atitle%20%3Ftitle%20%3B%0A%20foaf%3Amaker%20%3Fmaker%20%3B%0A%20dct%3Acreated%20%3Fcreated%20%3B%0A%20ubbont%3AmadeAfter%20%3FmadeAfter%20%3B%0A%20ubbont%3AmadeBefore%20%3FmadeBefore%20.%0A%7D%0AWHERE%20%7B%0A%20GRAPH%20%3Fg%20%7B%0A%20VALUES%20%3Fs%20%7B%20";
    var post = "%7D%0A%20%3Fs%20dct%3Aidentifier%20%3Fidentifier%20%3B%0A%20ubbont%3AhasThumbnail%20%3FhasThumbnail%20.%0A%20OPTIONAL%20%7B%20%3Fs%20foaf%3Amaker%2Ffoaf%3Aname%20%3Fmaker%20.%20%7D%0A%20OPTIONAL%20%7B%20%3Fs%20dct%3Acreated%20%3Fcreated%20.%20%7D%0A%20OPTIONAL%20%7B%20%3Fs%20dct%3Atitle%20%3Ftitle%20.%20%7D%0A%20OPTIONAL%20%7B%20%3Fs%20ubbont%3AmadeAfter%20%3FmadeAfter%20.%20%7D%0A%20OPTIONAL%20%7B%20%3Fs%20ubbont%3AmadeBefore%20%3FmadeBefore%20.%20%7D%0A%20%7D%0A%7D"
    var accept = "&output=json";
    var data = data.map(function (x) {
        return "<" + x + ">"
    });
    var query = sparql + pre + data.join('+') + post + accept;
    console.log("Query: " + query);
    var result = $.getJSON(query, function (rdf) {
        console.log("Result from endpont:");
        console.log(rdf);
        $('.json pre').append(JSON.stringify(rdf, undefined, 2));
    })
    return result;
};

semantic.api = {};

/* BETTER QUERY
 prefix foaf: <http://xmlns.com/foaf/0.1/>
 prefix dct: <http://purl.org/dc/terms/>
 prefix owl: <http://www.w3.org/2002/07/owl#>
 prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
 prefix ubbont: <http://data.ub.uib.no/ontology/>

 CONSTRUCT {
 ?s dct:identifier ?identifier ; 
 ubbont:hasThumbnail ?hasThumbnail ;
 foaf:maker ?maker ;
 dct:created ?created ;
 ubbont:madeAfter ?madeAfter ;
 ubbont:madeBefore ?madeBefore .
 }
 WHERE {
 GRAPH ?g {
 VALUES ?s { 


 }
 ?s dct:identifier ?identifier ;
 ubbont:hasThumbnail ?hasThumbnail .
 OPTIONAL { ?s foaf:maker/foaf:name ?maker . }
 OPTIONAL { ?s dct:created ?created . }
 OPTIONAL { ?s ubbont:madeAfter ?madeAfter . }
 OPTIONAL { ?s ubbont:madeBefore ?madeBefore . }
 }
 }
 */

// ready event
semantic.api.ready = function () {
    $(".get-oppdrag .button.submit").api({
        action: "get invoice",
        method: 'get',
        serializeForm: true,
        dataType: 'json',
        beforeSend: function (settings) {
            settings.urlData = {
                value: $('.value.input').val(),
                apikey: "d2ec241e152912ec3e9a3a5cdc91fd278739dd2c"
            };
            //console.log(settings.urlData);
            $('#invoiceUrl').attr('href', 'http://oppdrag.ub.uib.no/invoices/' + settings.urlData.value);
            return settings;
        },
        onResponse: function (response) {
            console.log(response);
            //Find url we want json-ld on
            var urls = findUrls(JSON.stringify(response.invoice.lines));
            console.log(urls)

            //Change domain to data.ub.uib.no
            dataUrls = urls.map(function (x) {
                return x.replace('marcus', 'data.ub')
            })
            var totalLines = response.invoice.lines.length;
            var urlLines = urls.length;
            $('.orderInfo').empty();
            $('.orderInfo').append( '<h3>' + response.invoice.custom_fields[0].value + '</h3>' );
            $('.orderInfo').append( '<p><strong>' + urlLines + '</strong> av <strong>' + totalLines + '</strong> ble funnet i Marcus.' );
            if(urlLines != totalLines) {
              $('.orderInfo').append('<div class="ui secondary inverted red segment">Denne bestillingen inneholder bilder vi ikke kunne finne i Marcus. Sjekk bestillingen og legg inn <i>url</i> i Oppdrag. </div>' );
            } 
            
            //Clear out the list if we check more than once
            $('.output ul').empty();

            //Just lest if the variables are ok
            for (var i = 0; i < urls.length; i++) {
                $('.output ul').append('<li>' + urls[i] + '</li>');
            }
            ;
            //$('.output').append("<br>===<br>");
            //$('.output').append(dataUrls);

            //Query the endpoint
            var jsonLdResponse = getJsonld(dataUrls);

            //Clear json preview if we check more than once
            $('.json pre').empty();

            jsonLdResponse.done(function (x) {
                //Preview the response
                $('.json pre').append(JSON.stringify(x, undefined, 2));

                //Send to printLabel.js for printer check, preview and print
                //dymoPreviewAndPrint(x);
                initializeVariables(x);
            });

            response = jsonLdResponse;
            return response;
        },
        onError: function (err) {
            $('.output').text("404");
        }
    });
};


// attach ready event
$(document)
    .ready(semantic.api.ready)
;