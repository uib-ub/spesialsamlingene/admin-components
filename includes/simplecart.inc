			<div class="simpleCart_shelfItem well">
				<form role="form">
				    <div class="row">
				    <input class="item_name" hidden value="{{ first.main.title.value }}">
				    <input class="item_link" hidden value="{{ uri }}">
				    <input class="item_thumb" hidden value="{{ first.photograph.img.value }}">
				    <input class="item_sign" hidden value="{{ first.main.identifier.value }}">
					<div class="col-md-4">
					<input type="number" value="1" min="1" max="50" style="width: 100%;" class="form-control item_Quantity" required="true"> 
				    </div>
				    <div class="col-md-8">
				    <select class="form-control item_type">
                        <option value="error">Velg produkt</option>
					    <option value="13x18">Trykk 13x18 - 100kr</option>
					    <option value="18x24">Trykk 18x24 - 150kr</option>
					    <option value="24x30">Trykk 24x30 - 200kr</option>
					    <option value="30x40">Trykk 30x40 - 250kr</option>
					    <option value="40x50">Trykk 40x50 - 350kr</option>
					    <option value="50x60">Trykk 50x60 - 450kr</option>
					    <option value="60x70">Trykk 60x70 - 550kr</option>
					    <option value="publikasjon">Bruk i publisering - 500kr</option>
				    </select>
				    </div>
				    </div>
				    <br>
				    <div class="row">
				    <div class="col-md-5"><span class="item_price"></span></div>
				    <a class="btn btn-primary btn-sm col-md-6 item_add" href="javascript:;"> Legg i handlekurv </a>
				    </div>
				</form>
			</div>