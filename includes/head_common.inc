<meta charset="utf-8">
<title>{{lodspk.title}} {% if not first.main.title.value | null %} - {{first.main.title.value}}{% else %} - {% if not first.main.label.value | null %}{{first.main.label.value}}{%endif%}{%endif%}</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="//img/favicon-adm.ico" type="image/x-icon">
<link rel="icon" href="//img/favicon-adm.ico" type="image/x-icon">

<!-- <script type="text/javascript" src="{{lodspk.home}}js/jquery.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{lodspk.home}}semantic/dist/semantic.min.css">
<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">-->
<link rel="stylesheet" type="text/css" href="{{lodspk.home}}css/marcus_overrides.css">
<link rel="stylesheet" type="text/css" href="{{lodspk.home}}css/print.css" media="print">

<script src="{{lodspk.home}}semantic/dist/semantic.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.1/semantic.min.js"></script>-->
<script src="{{lodspk.home}}js/marcus-ux.js"></script>
<script src="{{lodspk.home}}js/holder.js"></script>

<!-- Font Awesome -->
<!--<link href="//netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">-->
<!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/fontawesome.min.css"/>-->

<!-- Leaflet -->
<link rel="stylesheet" href="{{lodspk.home}}node_modules/leaflet/dist/leaflet.css">
<script src="{{lodspk.home}}node_modules/leaflet/dist/leaflet.js"></script>
<script src="{{lodspk.home}}js/Leaflet/leaflet-providers.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!-- TODO: Move to pages using it -->
<script type="text/javascript" src="{{lodspk.home}}js/qrcode/jquery.qrcode-0.7.0.min.js"></script>

<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Libre+Baskerville|Montserrat:400,400i,700" rel="stylesheet"> 

