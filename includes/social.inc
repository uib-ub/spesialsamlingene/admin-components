			<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
			<script type="text/javascript">
			(function(d){
			    var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
			    p.type = 'text/javascript';
			    p.async = true;
			    p.src = '//assets.pinterest.com/js/pinit.js';
			    f.parentNode.insertBefore(p, f);
			}(document));
			</script>

			<ul class="list-inline social">
				<li><a href='https://twitter.com/intent/tweet?text={% if not first.main.title | null %}{{first.main.title.value}}{% else %}{% if not first.main.label | null %}{{first.main.label.value}}{%endif%}{%endif%} Fra marcus.uib.no&url={{lodspk.local.value}}'><i class="fa fa-twitter fa-2x"></i></a></li>
				<li><a href="https://www.facebook.com/sharer/sharer.php?u={{lodspk.local.value}}" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a></li>
				<li><a href="http://www.pinterest.com/pin/create/button/?url={{lodspk.local.value}}&media={% for row in models.photograph %}{% if 'lg.jpg' in row.img.value %}{{row.img.value}}{% endif %}{% endfor %}&description={% if first.main.title != '' %}{{first.main.title.value}}{% else %}{% if  not first.main.label | null %}{{first.main.label.value}}{%endif%}{%endif%} Fra marcus.uib.no" data-pin-do="buttonPin" data-pin-height="16"></i></a></li>
				
			</ul>
 
<!-- THE URL TO THE RESOURCE MUST BE CHANGED WHEN GOING TO PRODUCTON (value to curie)-->