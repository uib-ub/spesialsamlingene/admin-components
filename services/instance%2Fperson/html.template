<!DOCTYPE html>
<html lang="no" prefix="og: http://ogp.me/ns#">
<head>
  {%include "../../includes/head_common.inc"%}
  {%include "../../includes/head_page.inc"%}
</head>
<body>
  {%include "../../includes/sidebar.inc"%}
  <div class="pusher">
    {%include "../../includes/static_sidebar_left.inc"%}
    <div class="ui segment">
      <div class="main-content-wrapper">
        {%include "../../includes/header.inc"%}

        <div class="main-content">
          <div class="ui grid">  
            <div class="ui sixteen wide column">
              <h1>{{first.main.label.value}}</h1>
              <span class="ui label">{{first.count.total.value}}</span>

              {% if first.main.description.value %}<p itemprop="http://schema.org/description">{{first.main.description.value}}</p>{% endif %}

              <p>{% if first.main.altname %}Også kjent som: <ul class="list-inline text-muted">{% for row in models.main %}{% ifchanged row.label.value %}<li><i>{{row.altname.value}}</i></li>{% endifchanged %}{% endfor %}</ul>{% endif %}</p>

              {% if not first.dbpedia.sameas.dbparticle | null %}
              <div class="ui clearing segment">
                {% for i in models.dbpedia.sameas %}
                {% ifchanged i.dbparticle.value %}
                <img class="ui small right floated image" src="{{i.thumbnail.value}}">
                <p>{% autoescape off %}{{i.abstract.value}}{% endautoescape %}<p>
                <p><i class="fa fa-wikipedia-w"></i> Fra Wikipedia (<a href="{{i.dbparticle.value}}"><small>{{i.dbparticle.value}}</small></a>)</p>
                {% endifchanged %}
                {% endfor %}
              </div>
              {% endif %}

                {% if models.ms|length != 0 %}
                <div class="ui segment">
                <p><strong>{{ first.main.label.value }} er relatert til disse samlingene.</strong> Disse samlingene er ikke tilgjengelig i Marcus, men du kan finne mer informasjon i den gamle <a href="http://www.uib.no/filearchive/manuskriptkatalogen-ms-1-2097.pdf" target="_blank">Manuskriptkatalogen (pdf)</a>.</p>
                  <div class="ui horizontal list">
                    {% for row in models.ms %}
                    <div class="item"><i class="icon fa fa-dot-circle-o"></i> {{row.label.value}}</div>
                    {% endfor %}
                  </div>
                </div>
                {% endif%}


                {% if models.collections|length != 0 %}
                <div class="ui items">
                  <h3>Relaterte samlinger</h3>
                  {% for row in models.collections %}

                  {% ifchanged row.uri.value %}
                  <div style="item">

                  <div class="content">
                    <h4><i class="fa fa-archive"></i><a href="{{ row.uri.value }}"> {% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}</a></h4>

                    {% if row.description.value %}
                    <div class="description">
                    <p>{{row.description.value|truncatewords:35}}</p>
                    {% endif %}
                    </div>

                    <p><span class="ui label">Samling</span> <span><i class="fa fa-key"></i>{{row.identifier.value}}</span></p>
                    </div>
                  </div>
                  {% endifchanged %}

                  {% endfor %}
                </div>
                {% endif%}

              </div>
            </div>

            <div class="equal width row">
              <div class="three wide column">
                
                <img class="ui circular image" src="{{first.main.img.value}}">
                
                <div class="ui list">
                  {% if first.main.birthdate %}<div class="item"><i class="icon asterisk"></i><div class="content">{{first.main.birthdate.value}}</div></div>{% endif %}
                  {% if first.main.deathdate %}<div class="item"><i class="icon"><strong>†</strong></i><div class="content">{{first.main.deathdate.value}}</div></div>{% endif %}
                  {% if first.main.profession %}<div class="item"><i class="icon briefcase"></i><div class="content">{{first.main.profession.value}}</div></div>{% endif %}
                </div>

                {% if first.main.birthplaceuri %}
                <p><strong class="text-muted">Fødested</strong><br>
                  <a href="{{first.main.birthplaceuri.value}}">{{first.main.birthplace.value}}</a>
                </p>
                {% endif %}

                {% if first.main.deathplaceuri %}
                <p><strong class="text-muted">Dødssted</strong><br>
                  <a href="{{first.main.deathplaceuri.value}}">{{first.main.deathplace.value}}</a>
                </p>
                {% endif %}

                {% if first.main.spouseUri %}
                <p><strong class="text-muted">Ektefelle</strong><br>
                  <a href="{{first.main.spouseUri.value}}">{{first.main.spouseName.value}}</a>
                </p>
                {% endif %}

                {% if models.parents|length != 0 %}
                <p><strong class="text-muted">Foreldre</strong></p>
                <ul class="ui list">
                  {% for row in models.parents %}
                  <li><a href="{{row.uri.value}}">{{row.name.value}}</a></li>
                  {% endfor %}
                </ul>
                {% endif %}

                {% if models.children|length != 0 %}
                <p><strong class="text-muted">Barn</strong></p>
                <ul class="ui list">
                  {% for row in models.children %}
                  <li><a href="{{row.uri.value}}">{{row.name.value}}</a></li>
                  {% endfor %}
                </ul>
                {% endif %}

                {% if models.webresources|length != 0 %}
                <p><strong class="text-muted">Nettsider</strong><br></p>
                <div class="ui relaxed divided list">
                  {% for row in models.webresources %}
                   <div clas="item">
                     <div class="content">
                       <a target="_blank" href="{{row.uri.value}}">{{row.label.value}}</a> <i class="fa fa-external-link"></i><hr />
                    </div>
                  </div>
                  {% endfor %}
                </div>
                {% endif %}


              <div class="column">

                {% if first.count.total.value > 100 %}
                <div class="ui borderless pagination menu">
                  <script>
                    if ("{{lodspk.args.arg1}}".length < 1)
                      {page = 0;}
                    else {page = "{{lodspk.args.arg1}}";}
                    if ("{{lodspk.args.arg0}}".length > 0)
                      {page=parseInt(page)}
                    else {}
                      var offset = 50;
                    var count = {{first.count.total.value}};
                    var totalPages = Math.floor(count / 50);
                    var lastPage = '{{lodspk.home}}instance/person/{{lodspk.args.arg0}}/'+ (count - ((count - (50*totalPages))));

                    var nextPage = page+offset;
                    var prevPage = page-offset;
                    var next_uri = '{{lodspk.home}}instance/person/{{lodspk.args.arg0}}/'+ nextPage;
                    var prev_uri = '{{lodspk.home}}instance/person/{{lodspk.args.arg0}}/'+ prevPage;
                    if (prevPage === 0){
                      document.write('<div class="item"><a href="{{lodspk.home}}instance/person/{{lodspk.args.arg0}}" id="prev"><i class="icon left arrow"></i> Forrige 50</a></div>'); 
                    }   
                    else if ("{{lodspk.args.arg1}}".length > 1){
                      document.write('<div class="item"><a href="{{lodspk.home}}instance/person/{{lodspk.args.arg0}}/">Første side</a></div><div class="item"><a href="" class="prev">Forrige 50</a></div>');
                      $(".prev").attr("href",prev_uri);
                    };

                    if ({{first.count.total.value}} > nextPage){
                      document.write('<div class="item"><a href="" class="next">Neste 50</a></div><div class="item"><a class="lastpage" href="">Siste side <i class="icon right arrow"></i></a></div>');
                      $(".next").attr("href",next_uri);
                      $(".lastpage").attr("href",lastPage);
                    };
                  </script> 
                </div>
                {% endif %}

                <div class="ui six doubling cards">
                  {% for row in models.related %}
                  {% ifchanged row.uri.value %}
                  <div class="card">
                    {% if row.classLabel.value != "Samling" %}
                    <a class="ui medium image" href="{{row.uri.value}}"><img {% if row.img %}{% else %}data-src="holder.js/100%x150/text:Ingen forhåndsvisning"{% endif %} src="{{row.img.value}}" /></a>
                    {% endif %}
                    {% if row.classLabel.value == "Samling" %}
                    <a class="ui medium image" style="color:#bbb;" href="{{row.uri.value}}">{% if row.img %}<img src="{{row.img.value}}" />{% else %}<span class="right floated fa-stack fa-3x">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-archive fa-stack-1x fa-inverse"></i>
                    </span>{% endif %}</a>
                    {% endif %}
                    <div class="content">
                      <a class="header" href="{{ row.uri.value }}">{% if row.title && row.title.value != "" %}{{row.title.value}}{% else %}{% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}{%endif%}</a>
                      <div class="meta">
                        {% if row.makerNames %}
                        <p><i style="margin-right: 5px;" data-trigger="hover" data-toggle="tooltip" data-placement="left" title="Personer knyttet til samlingen eller dokumentet" class="fa fa-users"></i> <strong>{{row.makerNames.value}}</strong></p>
                        {% endif %}
                      </div>

                      {% if row.description.value %}
                      <div class="description">
                        <p>{{row.description.value|truncatewords:35}}</p>
                      </div>{% endif %}
                      <div class="extra">
                        {% if row.subjectLabels %}
                        <p class="text-muted"><i class="fa fa-tags"></i> <i itemprop="keywords">{{row.subjectLabels.value}}</i></p>
                        {% endif %}


                        <p> {% if row.classLabel %}<span class="ui label">{{row.classLabel.value}}</span> {% endif %} {% if row.hasTranscription.value == "true" %}<span class="ui label">Transkribert</span>{% endif %} {% if row.hasPage.value == "true" %}<span class="ui label">Faksimile</span>{% endif %} <span ><i class="fa fa-key"></i>{{row.identifier.value}}</span></p>
                        {% if row.show.value ==  "false" %}<p><span class="ui red label">Upublisert</span></p>{% endif %}
                        {% if row.show.value == "true" %}<p><span class="ui blue label">Publisert</span></p>{% endif %}
                        {% if !row.show %}<p><span class="ui red label">Upublisert</span></p>{% endif %}
                      </div>
                    </div>
                  </div>
                  {% endifchanged %}    
                  {% endfor %}
                </div>

                {% if first.count.total.value > 100 %}
                <div class="ui borderless pagination menu">
                  <script>
                    if ("{{lodspk.args.arg1}}".length < 1)
                      {page = 0;}
                    else {page = "{{lodspk.args.arg1}}";}
                    if ("{{lodspk.args.arg0}}".length > 0)
                      {page=parseInt(page)}
                    else {}
                      var offset = 50;
                    var count = {{first.count.total.value}};
                    var totalPages = Math.floor(count / 50);
                    var lastPage = '{{lodspk.home}}instance/person/{{lodspk.args.arg0}}/'+ (count - ((count - (50*totalPages))));

                    var nextPage = page+offset;
                    var prevPage = page-offset;
                    var next_uri = '{{lodspk.home}}instance/person/{{lodspk.args.arg0}}/'+ nextPage;
                    var prev_uri = '{{lodspk.home}}instance/person/{{lodspk.args.arg0}}/'+ prevPage;
                    if (prevPage === 0){
                      document.write('<div class="item"><a href="{{lodspk.home}}instance/person/{{lodspk.args.arg0}}" id="prev"><i class="icon left arrow"></i> Forrige 50</a></div>'); 
                    }   
                    else if ("{{lodspk.args.arg1}}".length > 1){
                      document.write('<div class="item"><a href="{{lodspk.home}}instance/person/{{lodspk.args.arg0}}/">Første side</a></div><div class="item"><a href="" class="prev">Forrige 50</a></div>');
                      $(".prev").attr("href",prev_uri);
                    };

                    if ({{first.count.total.value}} > nextPage){
                      document.write('<div class="item"><a href="" class="next">Neste 50</a></div><div class="item"><a class="lastpage" href="">Siste side <i class="icon right arrow"></i></a></div>');
                      $(".next").attr("href",next_uri);
                      $(".lastpage").attr("href",lastPage);
                    };
                  </script> 
                </div>
                {% endif %}
              </div>
            </div>

            <div class="row">
              <div class="column">
                <!-- ALL DATA MODAL -->
                {% include "../../includes/all-data-modal.inc" %}
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    {%include "../../includes/footer.inc"%}

  </body>
  </html>
