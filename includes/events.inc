<div class="ui seven doubling cards">
  {% for row in models.events %}
  <div class="card">
    {% if row.logo %}
    <a class="ui small image" href="{{row.uri.value}}">
      <img src="{{row.logo.value}}"/>
    </a>
    {% else %}
    <a href="{{row.uri.value}}" class="ui small image">
      <img data-src="holder.js/100x100">
    </a>
    {% endif %}
    <div class="middle aligned content">
      <a class="header" href="{{row.uri.value}}">{% if row.title && not row.title.value | null %}{{row.title.value}}{% else %}{% if row.label && not row.label.value | null %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}{%endif%}</a>
     <!-- {% if row.description %}
      <div class="description">
        <p>{{row.description.value|truncatewords:25}}</p>
        <a href="{{row.uri.value}}">se mer</a>
      </div>
      {%endif%}-->
    </div>
  </div>
  {% endfor %}
</div>