					<ul class="nav in" id="side-menu">
                        <li>
                            <a href="{{lodspk.home}}home"><i style="color: black;" class="fa fa-dashboard fa-fw"></i> Kontrollpanel</a>
                        </li>
                        <li>
                            <a href="{{lodspk.home}}cataloguers"><i style="color: black;" class="fa fa-check-square-o fa-fw"></i> Siste 500</a>
                        </li>
                        <li>
                            <a href="/upload/"><i style="color: black;" class="fa fa-upload fa-fw"></i> Last opp filer</a>
                        </li>
                        <li>
                            <a href="{{lodspk.home}}new-resource"><i style="color: black;" class="fa fa-plus fa-fw"></i> Filer klare for registrering</a>
                        </li>
                        <li>
                            <a href="{{lodspk.home}}add-metadata"><i style="color: black;" class="fa fa-edit fa-fw"></i> Objekt klare for publisering</a>
                        </li>
                        <li>
                            <a href="{{lodspk.home}}unpublished-with-dzi"><i style="color: black;" class="fa fa-edit fa-fw"></i> Upubliserte med nye filer</a>
                        </li>
                        <li>
                            <a href="{{lodspk.home}}topics-hierarchy"><i style="color: black;" class="fa fa-tags fa-fw"></i> Emneregister - oversikt</a>
                        </li>
                        <li>
                        	<a href="#">
                        	<i style="color: black;" class="fa fa-wrench fa-fw"></i> Husholdning <span style="color: black;" class="fa arrow"></span>
                        	</a>
		                        <ul class="nav nav-second-level collapse in">
			                        <li>
			                            <a href="{{lodspk.home}}orders"><i style="color: black;" class="fa fa-search fa-fw"></i> Bestillinger</a>
			                        </li>
									<li>
			                            <a href="{{lodspk.home}}worklists"><i style="color: black;" class="fa fa-edit fa-fw"></i> Arbeidslister</a>
			                        </li>
		                        	<li>
			                            <a href="{{lodspk.home}}acquisitions"><i style="color: black;" class="fa fa-gift fa-fw"></i> Akkvisisjoner</a>
			                        </li>
			                        <li>
			                            <a href="{{lodspk.home}}storageunits"><i style="color: black;" class="fa fa-bank fa-fw"></i> Lagringsenheter</a>
			                        </li>
			                    </ul>
			            </li>
                        <li>
                            <a href="{{lodspk.home}}errors"><i style="color: black;" class="fa fa-exclamation-triangle fa-fw"></i> Feil i datasettet</a>
                        </li>                        
                        <li>
                            <a href="{{lodspk.home}}marcus-statistics"><i style="color: black;" class="fa fa-compass fa-fw"></i> Statistikk for Marcus</a>
                        </li>
                        <li>
                            <a href="{{lodspk.home}}stats"><i style="color: black;" class="fa fa-compass fa-fw"></i> Statistikk for datasettet</a>
                        </li>
                        <li>
                            <a href="{{lodspk.home}}manual"><i style="color: black;" class="fa fa-book fa-fw"></i> Veiledning</a>
                        </li>
                    </ul>
