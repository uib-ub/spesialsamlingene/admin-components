    <div class="main-content-menu ui fixed secondary menu">
      <div class="item">
        <div id="show-menu">
          <a class="item ui mobile only"><i class="large sidebar icon"></i></a>
        </div>
        <div class="logo">
          <a href="{{lodspk.home}}home">Marcus<span class="descr">-admin</span></a>
        </div>
      </div>
       <div class="item">

          <form class="ui form" role="search" method="get" action="/search">
            <input type="text" placeholder="Søk ..." name="q">
          </form>
        </div>
      </div>
